/*
 * @Author: jianfanfan 1217572833@qq.com
 * @Date: 2023-11-20 16:53:28
 * @LastEditors: jianfanfan 1217572833@qq.com
 * @LastEditTime: 2023-12-04 17:44:08
 * @FilePath: \nest-project\src\app.module.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { LoginModule } from './modules/login/login.module';
import { UserModule } from './modules/user/user.module';
import { UserextendModule } from './modules/userextend/userextend.module';
import { NewsModule } from './modules/news/news.module';
import { Login } from './modules/login/entities/login.entity';
import { User } from './modules/user/entities/user.entity';
import { UserExtend } from './modules/userextend/entities/userextend.entity';
import { News } from './modules/news/entities/news.entity';
import { LoggerModule } from './logger/logger.module';
import { ModulesController } from './modules/modules/modules.controller';
import { ProfileModule } from './modules/profile/profile.module';
import { Profile } from './modules/profile/entities/profile.entity';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DATABASE_HOST,
      port: parseInt(process.env.DATABASE_PORT, 10),
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_NAME,
      entities: [Login, User, UserExtend, News, Profile],
      synchronize: true,
    }),
    LoginModule,
    UserModule,
    UserextendModule,
    LoggerModule,
    NewsModule,
    ProfileModule,
  ],
  controllers: [AppController, ModulesController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
