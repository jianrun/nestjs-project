/*
 * @Author: jianfanfan 1217572833@qq.com
 * @Date: 2023-11-22 15:28:54
 * @LastEditors: jianfanfan 1217572833@qq.com
 * @LastEditTime: 2023-11-22 15:29:06
 * @FilePath: \nest-project\src\logger\logger.module.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { Module } from '@nestjs/common';
import { LoggerService } from './logger.service';

@Module({
  providers: [LoggerService],
  exports: [LoggerService],
})
export class LoggerModule {}
