import { Injectable } from '@nestjs/common';
import * as winston from 'winston';
import * as DailyRotateFile from 'winston-daily-rotate-file';

@Injectable()
export class LoggerService {
  private logger: winston.Logger;

  constructor() {
    const logFormat = winston.format.printf(({ level, message, timestamp }) => {
      return `${timestamp} [${level.toUpperCase()}]: ${message}`;
    });

    const transport = new DailyRotateFile({
      filename: 'logs/%DATE%-log.log',
      datePattern: 'YYYY-MM-DD',
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '14d',
      level: process.env.NODE_ENV === 'production' ? 'info' : 'debug',
      format: winston.format.combine(winston.format.timestamp(), logFormat),
    });

    this.logger = winston.createLogger({
      transports: [transport, new winston.transports.Console()],
    });
  }

  log(message: string): void {
    this.logger.info(message);
  }

  error(message: string, trace: string): void {
    this.logger.error(message, { trace });
  }
}
