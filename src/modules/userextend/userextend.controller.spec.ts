import { Test, TestingModule } from '@nestjs/testing';
import { UserextendController } from './userextend.controller';
import { UserextendService } from './userextend.service';

describe('UserextendController', () => {
  let controller: UserextendController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserextendController],
      providers: [UserextendService],
    }).compile();

    controller = module.get<UserextendController>(UserextendController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
