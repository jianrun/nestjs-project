import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { UserextendService } from './userextend.service';
import { CreateUserextendDto } from './dto/create-userextend.dto';
import { UpdateUserextendDto } from './dto/update-userextend.dto';

@Controller('userextend')
export class UserextendController {
  constructor(private readonly userextendService: UserextendService) {}

  @Post()
  create(@Body() createUserextendDto: CreateUserextendDto) {
    return this.userextendService.create(createUserextendDto);
  }

  @Get()
  findAll() {
    return this.userextendService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.userextendService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserextendDto: UpdateUserextendDto) {
    return this.userextendService.update(+id, updateUserextendDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.userextendService.remove(+id);
  }
}
