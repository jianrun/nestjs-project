/*
 * @Author: jianfanfan 1217572833@qq.com
 * @Date: 2023-11-21 17:54:20
 * @LastEditors: jianfanfan 1217572833@qq.com
 * @LastEditTime: 2023-11-21 19:59:47
 * @FilePath: \nest-project\src\modules\userextend\dto\create-userextend.dto.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
export class CreateUserextendDto {
  mobile = '13412345678';
  address = '中国';
}
