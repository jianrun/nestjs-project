import { PartialType } from '@nestjs/mapped-types';
import { CreateUserextendDto } from './create-userextend.dto';

export class UpdateUserextendDto extends PartialType(CreateUserextendDto) {}
