/*
 * @Author: jianfanfan 1217572833@qq.com
 * @Date: 2023-11-21 17:54:20
 * @LastEditors: jianfanfan 1217572833@qq.com
 * @LastEditTime: 2023-11-21 19:59:14
 * @FilePath: \nest-project\src\modules\userextend\userextend.service.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { Injectable } from '@nestjs/common';
import { CreateUserextendDto } from './dto/create-userextend.dto';
import { UpdateUserextendDto } from './dto/update-userextend.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserExtend } from './entities/userextend.entity';

@Injectable()
export class UserextendService {
  constructor(
    @InjectRepository(UserExtend)
    private userExtendRepository: Repository<UserExtend>,
  ) {}

  /**
   * 新增一条新的数据
   * @param createUserDto
   * @returns
   */
  async create(CreateUserextendDto: CreateUserextendDto): Promise<UserExtend> {
    const newUser = this.userExtendRepository.create(CreateUserextendDto);
    return await this.userExtendRepository.save(newUser);
  }

  findAll() {
    return `This action returns all userextend`;
  }

  findOne(id: number) {
    return `This action returns a #${id} userextend`;
  }

  update(id: number, updateUserextendDto: UpdateUserextendDto) {
    return `This action updates a #${id} userextend`;
  }

  remove(id: number) {
    return `This action removes a #${id} userextend`;
  }
}
