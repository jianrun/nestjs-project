import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserextendService } from './userextend.service';
import { UserextendController } from './userextend.controller';
import { UserExtend } from './entities/userextend.entity';

@Module({
  imports: [TypeOrmModule.forFeature([UserExtend])],
  controllers: [UserextendController],
  providers: [UserextendService],
})
export class UserextendModule {}
