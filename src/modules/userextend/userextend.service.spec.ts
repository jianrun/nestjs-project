import { Test, TestingModule } from '@nestjs/testing';
import { UserextendService } from './userextend.service';

describe('UserextendService', () => {
  let service: UserextendService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserextendService],
    }).compile();

    service = module.get<UserextendService>(UserextendService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
