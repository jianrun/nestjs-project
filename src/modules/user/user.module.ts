/*
 * @Author: jianfanfan 1217572833@qq.com
 * @Date: 2023-11-21 17:51:59
 * @LastEditors: jianfanfan 1217572833@qq.com
 * @LastEditTime: 2023-11-21 19:51:41
 * @FilePath: \nest-project\src\modules\user\user.module.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { User } from './entities/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
