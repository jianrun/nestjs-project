/*
 * @Author: jianfanfan 1217572833@qq.com
 * @Date: 2023-11-21 17:51:59
 * @LastEditors: jianfanfan 1217572833@qq.com
 * @LastEditTime: 2023-11-21 19:28:25
 * @FilePath: \nest-project\src\modules\user\dto\create-user.dto.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
export class CreateUserDto {
  username: '王五';
  password: '123456';
  mobile: '18872481984';
  address: '中国';
}
