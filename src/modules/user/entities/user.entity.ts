/*
 * @Author: jianfanfan 1217572833@qq.com
 * @Date: 2023-11-21 17:51:59
 * @LastEditors: jianfanfan 1217572833@qq.com
 * @LastEditTime: 2023-12-04 17:29:02
 * @FilePath: \nest-project\src\modules\user\entities\user.entity.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';
import { UserExtend } from 'src/modules/userextend/entities/userextend.entity';
import { Profile } from 'src/modules/profile/entities/profile.entity';

@Entity({ name: 'user' })
export class User {
  @PrimaryGeneratedColumn({
    type: 'int',
    name: 'id',
    comment: '主键id',
  })
  id: number;

  @Column({
    type: 'varchar',
    nullable: false,
    length: 50,
    unique: true,
    name: 'username', // 如果是一样的可以不指定
    comment: '用户名',
  })
  username: string;

  @Column({
    type: 'varchar',
    nullable: false,
    length: 100,
    comment: '密码',
  })
  password: string;

  @Column('tinyint', {
    nullable: false,
    default: () => 0,
    name: 'is_del',
    comment: '是否删除,1表示删除,0表示正常',
  })
  isDel: number;

  @CreateDateColumn({
    type: 'timestamp',
    nullable: false,
    name: 'created_at', // mysql数据库规范是使用下划线命名的,不使用驼峰
    comment: '创建时间',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    nullable: false,
    name: 'updated_at',
    comment: '更新时间',
  })
  updateAt: Date;

  @OneToOne((type) => UserExtend, (userExtend) => userExtend.user)
  userDetail: UserExtend;

  @OneToMany(() => Profile, (Profile) => Profile.user)
  profile: Profile[];
}
