import { Injectable } from '@nestjs/common';
import { CreateNewsDto } from './dto/create-news.dto';
import { UpdateNewsDto } from './dto/update-news.dto';
import { News } from './entities/news.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class NewsService {
  constructor(
    @InjectRepository(News)
    private newsRepository: Repository<News>,
  ) {}
  create(createNewsDto: CreateNewsDto) {
    return 'This action adds a new news';
  }

  async findAll(
    page: number = 1,
    pageSize: number = 10,
  ): Promise<[News[], number]> {
    const skip = (page - 1) * pageSize;
    const res = await this.newsRepository.findAndCount({
      skip,
      take: pageSize,
    });
    const [items, total] = res;
    console.log('res', res);
    return [items, total];
  }

  findOne(id: number) {
    return `This action returns a #${id} news`;
  }

  update(id: number, updateNewsDto: UpdateNewsDto) {
    return `This action updates a #${id} news`;
  }

  remove(id: number) {
    return `This action removes a #${id} news`;
  }
}
