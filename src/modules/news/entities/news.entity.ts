/*
 * @Author: jianfanfan 1217572833@qq.com
 * @Date: 2023-11-22 16:15:12
 * @LastEditors: jianfanfan 1217572833@qq.com
 * @LastEditTime: 2023-11-27 10:29:00
 * @FilePath: \nest-project\src\modules\news\entities\news.entity.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'news' })
export class News {
  @PrimaryGeneratedColumn({
    type: 'int',
    name: 'id',
    comment: '主键id',
  })
  id: number;

  @Column({
    type: 'int',
    default: () => 0,
    comment: '类型（1：行业新闻，2：公司新闻）',
    name: 'news_type',
  })
  newsType: number;

  @Column({
    type: 'varchar',
    length: 80,
    comment: '标题',
    name: 'title',
  })
  title: string;

  @Column({
    type: 'bigint',
    comment: '图片',
    name: 'image_id',
  })
  imageId: string;

  @Column({
    type: 'varchar',
    length: 500,
    comment: 'seo关键词',
    name: 'seo_keyword',
  })
  seoKeyword: string;

  @Column({
    type: 'text',
    comment: '新闻详情',
    name: 'content',
  })
  content: string;

  @Column({
    type: 'int',
    default: () => 0,
    comment: '状态（0：禁用，1：启用，2：已删除）',
    name: 'status',
  })
  status: number;

  @Column({
    type: 'int',
    comment: '创建人',
    name: 'create_user',
  })
  createUser: string;

  @Column({
    type: 'timestamp',
    comment: '创建时间',
    name: 'create_time',
  })
  createTime: Date;

  @Column({
    type: 'bigint',
    comment: '修改人',
    name: 'update_user',
  })
  updateUser: string;

  @Column({
    type: 'timestamp',
    comment: '修改时间',
    name: 'update_time',
  })
  updateTime: Date;

  @Column({
    type: 'varchar',
    comment: '副标题',
    name: 'short_info',
  })
  shortInfo: string;

  @Column({
    type: 'int',
    comment: '是否置顶',
    default: () => 0,
    name: 'is_top',
  })
  isTop: number;
}
