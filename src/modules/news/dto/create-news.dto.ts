/*
 * @Author: jianfanfan 1217572833@qq.com
 * @Date: 2023-11-22 16:15:11
 * @LastEditors: jianfanfan 1217572833@qq.com
 * @LastEditTime: 2023-11-27 11:38:09
 * @FilePath: \nest-project\src\modules\news\dto\create-news.dto.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
export class CreateNewsDto {
  id: number;
  newsType: number;
}
