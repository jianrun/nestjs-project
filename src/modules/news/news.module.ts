/*
 * @Author: jianfanfan 1217572833@qq.com
 * @Date: 2023-11-22 16:15:11
 * @LastEditors: jianfanfan 1217572833@qq.com
 * @LastEditTime: 2023-11-27 10:27:14
 * @FilePath: \nest-project\src\modules\news\news.module.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { Module } from '@nestjs/common';
import { NewsService } from './news.service';
import { NewsController } from './news.controller';
import { News } from './entities/news.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([News])],
  controllers: [NewsController],
  providers: [NewsService],
})
export class NewsModule {}
