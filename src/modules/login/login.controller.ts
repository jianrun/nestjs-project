/*
 * @Author: jianfanfan 1217572833@qq.com
 * @Date: 2023-11-21 10:45:54
 * @LastEditors: jianfanfan 1217572833@qq.com
 * @LastEditTime: 2023-11-21 19:24:02
 * @FilePath: \nest-project\src\src\modules\login\login.controller.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Request,
} from '@nestjs/common';
import { LoginService } from './login.service';
import { CreateLoginDto } from './dto/create-login.dto';
import { UpdateLoginDto } from './dto/update-login.dto';

@Controller('auth')
export class LoginController {
  constructor(private readonly loginService: LoginService) {}

  @Post()
  async createUser(@Body() createLoginDto: CreateLoginDto) {
    await this.loginService.createLogin('username', 'password');
    return 'User registered successfully';
  }

  @Post('/login')
  login(@Body() createLoginDto: CreateLoginDto) {
    return this.loginService.login(createLoginDto);
  }

  @Post('register')
  async register(@Request() req) {
    const { username, password } = req.body;
    await this.loginService.createLogin(username, password);
    return 'User registered successfully';
  }

  @Get()
  findAll() {
    return this.loginService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.loginService.findOne(+id);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.loginService.remove(+id);
  }
}
