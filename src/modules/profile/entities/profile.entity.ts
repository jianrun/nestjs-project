/*
 * @Author: jianfanfan 1217572833@qq.com
 * @Date: 2023-12-04 16:55:28
 * @LastEditors: jianfanfan 1217572833@qq.com
 * @LastEditTime: 2023-12-04 17:38:33
 * @FilePath: \nest-project\src\modules\profile\entities\profile.entity.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { User } from 'src/modules/user/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn,
  ManyToOne,
} from 'typeorm';

@Entity({ name: 'profile' })
export class Profile {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  bio: string;

  @ManyToOne(() => User, (user) => user.profile)
  user: User;
}
